import axios from 'axios'

let http = axios.create({
  baseURL: 'https://hidden-depths-47488.herokuapp.com/api/',
  headers: {
    'Accept': 'application/json',
    'Content': 'application/json',
  }
})

export default http