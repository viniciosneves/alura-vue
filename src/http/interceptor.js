import http from './index'
import store from '../store'

export default function setup() {
  http.interceptors.request.use(function(config) {

    store.commit('requisicaoEmAdamento')
    if(store.getters.usuarioEstaLogado) {
      const token = store.getters.token
      config.headers.Authorization = `Bearer ${token}`
    }
    return config

  }, function(error) {
    if (error.response.status === 401) {
      store.commit('tokenInvalidoOuInexistente')
    }
    return Promise.reject(error)
  })

  http.interceptors.response.use((response) => {
    store.commit('requisicaoFinalizada')
    return response;
  }, (error) => {
    store.commit('requisicaoFinalizada')
    return Promise.reject(error);
  });

}