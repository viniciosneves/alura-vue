import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Login from '../views/Login'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      publica: true,
      somenteDeslogado: true
    }
  },
  {
    path: '/registre-se',
    name: 'registre-se',
    component: () => import(/* webpackChunkName: "registrar" */ '../views/Registrar.vue'),
    meta: {
      publica: true,
      somenteDeslogado: true
    }
  },
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/perfil',
    name: 'perfil',
    component: () => import(/* webpackChunkName: "perfil" */ '../views/Perfil.vue')
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  const rotaPublica = to.matched.some(record => record.meta.publica)
  const somenteDeslogado = to.matched.some(record => record.meta.somenteDeslogado)

  if (!rotaPublica && !store.getters.usuarioEstaLogado) {
    return next({
      path:'/login',
      query: {redirect: to.fullPath}
    });
  }
  if (store.getters.usuarioEstaLogado && somenteDeslogado) {
    return next('/')
  }

  next();
})

export default router
