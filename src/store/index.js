import Vue from 'vue'
import Vuex from 'vuex'
import router from '../router'
import http from '../http'

Vue.use(Vuex)

const TOKEN_KEY = 'access_token'

export default new Vuex.Store({
  state: {
    token: localStorage.getItem(TOKEN_KEY) || '',
    requisicao: null
  },
  mutations: {
    autenticado(state, token) {
      state.token = token
      localStorage.setItem(TOKEN_KEY, token)
    },
    falha_autenticacao(state) {
      state.status = 'auth.error'
    },
    deslogar(state) {
      state.token = ''
      localStorage.removeItem(TOKEN_KEY)
      router.push('/login')
    },
    tokenInvalidoOuInexistente() {
      this.commit('deslogar')
    },
    requisicaoEmAdamento(state) {
      state.requisicao = 'aguardando'
    },
    requisicaoFinalizada(state) {
      state.requisicao = 'finalizada'
    },
  },
  actions: {
    login({commit}, user){
      return new Promise((resolve, reject) => {
        http.post('login', user)
          .then(resp => {
            commit('autenticado', resp.data.token)
            resolve(resp)
          })
          .catch(err => {
            commit('falha_autenticacao')
            localStorage.removeItem('token')
            reject(err)
          })
      })
  },
  },
  getters: {
    usuarioEstaLogado: (state) => !!state.token,
    token: (state) => state.token,
    fazendoRequisicao: (state) => state.requisicao === 'aguardando'
  }
})
